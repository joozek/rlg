package pl.mjgsoft.spice.infra;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public abstract class AggregateRoot {
	private final List<Event> changes = new LinkedList<Event>();

	public List<Event> getUncommitedChanges() {
		return changes;
	}

	public abstract UUID getId();

	public void applyChanges(List<Event> eventsForAggregate) {
		for (Event event : eventsForAggregate) {
			applyChange(event, false);
		}
	}

	private void applyChange(Event event, boolean isNew) {
		try {
			Method apply = getClass().getDeclaredMethod("apply", event.getClass());
			apply.setAccessible(true);
			apply.invoke(this, event);
		} catch (IllegalAccessException e) {
			handle(e);
		} catch (InvocationTargetException e) {
			handle(e);
		} catch (NoSuchMethodException e) {
			handle(e);
		}

		if(isNew) {
			changes.add(event);
		}
	}

	protected void applyChange(Event event) {
		applyChange(event, true);
	}

	private void handle(Throwable e) {
		throw new RuntimeException(e);
	}
}
