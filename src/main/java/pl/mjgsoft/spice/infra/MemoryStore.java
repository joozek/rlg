package pl.mjgsoft.spice.infra;

import de.greenrobot.event.EventBus;

import java.util.*;

public class MemoryStore implements EventStore {
	private final EventBus bus;
	private final Map<UUID, List<EventEntry>> current = new HashMap<UUID, List<EventEntry>>();

	public MemoryStore(EventBus bus) {
		this.bus = bus;
	}

	@Override
	public void saveEvents(UUID aggregateId, List<Event> events, int expectedVersion) {
		List<EventEntry> entries;
		if (!current.containsKey(aggregateId)) {
			if(expectedVersion != -1) {
				throw new IllegalArgumentException("expectedVersion");
			}
			entries = new LinkedList<EventEntry>();
			current.put(aggregateId, entries);
		} else {
			entries = current.get(aggregateId);
			if(entries.get(entries.size()-1).version != expectedVersion && expectedVersion != -1) {
				throw new ConcurrencyException();
			}
		}

		for (Event event : events) {
			entries.add(new EventEntry(event, expectedVersion+1));
			bus.post(event);
		}
	}

	@Override
	public List<Event> getEventsForAggregate(UUID aggregateId) {
		if(!current.containsKey(aggregateId)) {
			throw new IllegalArgumentException("missing aggregate");
		} else {
			List<EventEntry> entries = current.get(aggregateId);
			ArrayList<Event> events = new ArrayList<Event>(entries.size());
			for (EventEntry entry : entries) {
				events.add(entry.event);
			}

			return events;
		}
	}

	private class EventEntry {
		public final int version;
		public final Event event;

		private EventEntry(Event event, int version) {
			this.event = event;
			this.version = version;
		}
	}
}
