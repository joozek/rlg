package pl.mjgsoft.spice.infra;

import java.util.UUID;

public class EventStoreRepository<T extends AggregateRoot> implements Repository<T> {
	private final EventStore storage;
	private final Class<T> clazz;

	public EventStoreRepository(EventStore eventStore, Class<T> clazz) {
		storage = eventStore;
		this.clazz = clazz;
	}

	@Override
	public void save(T aggregate, int expectedVersion) {
		storage.saveEvents(aggregate.getId(), aggregate.getUncommitedChanges(), expectedVersion);
	}

	@Override
	public T getById(UUID uuid) {
		try {
			T aggregate = clazz.newInstance();
			aggregate.applyChanges(storage.getEventsForAggregate(uuid));
			return aggregate;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
