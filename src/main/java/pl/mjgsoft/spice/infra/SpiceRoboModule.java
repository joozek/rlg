package pl.mjgsoft.spice.infra;

import com.google.inject.Binder;
import com.google.inject.Module;
import de.greenrobot.event.EventBus;

public class SpiceRoboModule implements Module {
	@Override
	public void configure(Binder binder) {
		binder.bind(EventBus.class).asEagerSingleton();
	}
}
