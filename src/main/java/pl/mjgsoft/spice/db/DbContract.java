package pl.mjgsoft.spice.db;

interface DbContract {
	interface Events {
		public static String TABLE_NAME = "Events";
		public static String ID = "Id";
		public static String BODY = "Body";
		public static String TIMESTAMP = "Timestamp";
	}
}
