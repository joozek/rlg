package pl.mjgsoft.spice.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Database {
	private final SQLiteDatabase db;

	public Database(Context context) {
		db = new DatabaseOpenHelper(context).getWritableDatabase();
	}

}
