package pl.mjgsoft.spice.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DatabaseOpenHelper extends SQLiteOpenHelper implements DbContract{

	private static int VERSION = 1;
	private static String NAME = "maindb";
	public DatabaseOpenHelper(Context context) {
		super(context, NAME,null,VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table "+Events.TABLE_NAME+"("
			+ Events.ID+" string primary key,"
			+ Events.BODY+" blob"
			+ Events.TIMESTAMP+" long"
			+ ")");
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
		//To change body of implemented methods use File | Settings | File Templates.
	}
}
