package pl.mjgsoft.spice.projections;

import java.util.UUID;

public class ProductListDto {
	public final String name;
	public final UUID id;
	public final int quantity;

	public ProductListDto(UUID id, String name) {
		this.id = id;
		this.name = name;
		quantity=0;
	}

	@Override
	public String toString() {
		return name;
	}
}
