package pl.mjgsoft.spice.projections;

import pl.mjgsoft.spice.events.ProductCreated;
import pl.mjgsoft.spice.infra.BullshitDb;

@SuppressWarnings("UnusedDeclaration")
public class ProductsListProjection {
	public void onEvent(ProductCreated e) {
		BullshitDb.productList.add(new ProductListDto(e.id, e.name));
	}
}
