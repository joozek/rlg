package pl.mjgsoft.spice.aggregates.product;

import pl.mjgsoft.spice.events.ProductCreated;
import pl.mjgsoft.spice.infra.AggregateRoot;

import java.util.UUID;

public class Product extends AggregateRoot {
	private UUID uuid;
	private String name;

	public Product(UUID uuid, String name) {
		applyChange(new ProductCreated(uuid, name));
	}

	@Override
	public UUID getId() {
		return uuid;
	}

	@SuppressWarnings("UnusedDeclaration")
	private void apply(ProductCreated e) {
		this.uuid = e.id;
		this.name = e.name;
	}
}
