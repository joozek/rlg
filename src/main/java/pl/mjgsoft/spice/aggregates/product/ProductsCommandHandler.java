package pl.mjgsoft.spice.aggregates.product;

import pl.mjgsoft.spice.commands.CreateProduct;
import pl.mjgsoft.spice.infra.Repository;

@SuppressWarnings("UnusedDeclaration")
public class ProductsCommandHandler {
	private final Repository<Product> repository;

	public ProductsCommandHandler(Repository<Product> repository) {
		this.repository = repository;
	}

	public void onEvent(CreateProduct event) {
		Product product = new Product(event.uuid, event.name);
		repository.save(product, -1);
	}
}
