package pl.mjgsoft.spice.events;

import pl.mjgsoft.spice.infra.Event;

import java.util.UUID;

public class ProductCreated implements Event {
	public final UUID id;
	public final String name;

	public ProductCreated(UUID id, String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return "ProductCreated{" +
			"id=" + id +
			", name='" + name + '\'' +
			'}';
	}
}
