package pl.mjgsoft.spice.commands;

import java.util.UUID;

public class CreateProduct {
	public final UUID uuid;
	public final String name;

	public CreateProduct(UUID uuid, String name) {
		this.uuid = uuid;
		this.name = name;
	}

	@Override
	public String toString() {
		return "CreateProduct{" +
			"uuid=" + uuid +
			", name='" + name + '\'' +
			'}';
	}
}
