package pl.mjgsoft.spice;

import android.app.Application;
import de.greenrobot.event.EventBus;
import pl.mjgsoft.spice.aggregates.product.Product;
import pl.mjgsoft.spice.infra.*;
import pl.mjgsoft.spice.projections.ProductsListProjection;
import pl.mjgsoft.spice.aggregates.product.ProductsCommandHandler;
import roboguice.RoboGuice;
import roboguice.util.Ln;

import javax.inject.Inject;

public class App extends Application {
	@Inject
	EventBus bus;

	@Override
	public void onCreate() {
		super.onCreate();
		RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE,
			RoboGuice.newDefaultRoboModule(this), new SpiceRoboModule());

		RoboGuice.injectMembers(this, this);
		bus.register(new LogAllEvents());
		bus.register(new ProductsListProjection());
		EventStore store = new MemoryStore(bus);
		Repository<Product> repository = new EventStoreRepository<Product>(store, Product.class);
		bus.register(new ProductsCommandHandler(repository));
		Ln.i("bus: %s", bus);
	}

	private class LogAllEvents {
		public void onEvent(Object e) {
			Ln.d("event: %s", e);
		}
	}
}
