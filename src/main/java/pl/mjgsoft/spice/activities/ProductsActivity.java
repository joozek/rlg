package pl.mjgsoft.spice.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import de.greenrobot.event.EventBus;
import pl.mjgsoft.spice.R;
import pl.mjgsoft.spice.commands.CreateProduct;
import pl.mjgsoft.spice.projections.ProductListDto;
import pl.mjgsoft.spice.services.ProductsService;
import roboguice.activity.RoboListActivity;

import javax.inject.Inject;
import java.util.Random;
import java.util.UUID;

public class ProductsActivity extends RoboListActivity {
	@Inject
	ProductsService productsService;
	@Inject
	Random random;
	@Inject
	EventBus bus;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		refresh();
	}

	private void refresh() {
		setListAdapter(new ArrayAdapter<ProductListDto>(this, R.layout.product_row, R.id.name, productsService.allProducts()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.products, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.add:
				addRandom();
				return true;
			case R.id.refresh:
				refresh();
				return true;
		}
		return super.onOptionsItemSelected(item);    //To change body of overridden methods use File | Settings | File Templates.
	}
	private void addRandom() {
		String name = "prod-" + random.nextInt(1000);
		bus.post(new CreateProduct(UUID.randomUUID(), name));
		refresh();
	}
}

