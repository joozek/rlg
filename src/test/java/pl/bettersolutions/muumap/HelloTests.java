package pl.bettersolutions.muumap;

import com.xtremelabs.robolectric.RobolectricTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.mjgsoft.spice.activities.ProductsActivity;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
public class HelloTests {
	private ProductsActivity helloAndroidActivity;

	@Before
	public void setUp() throws Exception {
		helloAndroidActivity = new ProductsActivity();
		helloAndroidActivity.onCreate(null);
	}

	@Test
	public void shouldHaveTitle() {
		assertThat(helloAndroidActivity.getTitle()).isEqualTo("Hello");
	}
}
