package pl.bettersolutions.muumap;

import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.ProtostuffIOUtil;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.runtime.RuntimeSchema;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class ProtoTests {

	@Test
	public void shouldSerializeAndDeserialize() {
		TestMsg joozek = new TestMsg(78, "joozek");
		Schema<TestMsg> schema = RuntimeSchema.getSchema(TestMsg.class);
		LinkedBuffer buffer = LinkedBuffer.allocate(100);
		byte[] bytes;
		try {
			bytes = ProtostuffIOUtil.toByteArray(joozek, schema, buffer);
		} finally {
			buffer.clear();
		}

		TestMsg joe = new TestMsg();
		ProtostuffIOUtil.mergeFrom(bytes,joe, schema);
		assertThat(joe).isEqualsToByComparingFields(joozek);
	}

	private class TestMsg {
		private int number;

		private TestMsg() {
		}

		private TestMsg(int number, String name) {
			this.number = number;
			this.name = name;
		}

		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		private String name;
	}
}
